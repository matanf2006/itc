"""
Written by Matan Feldman.
This module prints the HTMLs of several URLS, which are provided as a comma
separated list.
"""
import sys
import os
import requests


def main():
    """
    Receives a list of URLs as an input from the user, then print their HTMLs.
    :return:
    """
    if 'url_list' in os.environ.keys():
        url_list = str(os.environ['url_list']).split(',')
    else:
        url_list = str(sys.argv[1]).split(',')

    for url in url_list:
        html = requests.get(url).text
        print(html, '\n\n')


if __name__ == '__main__':
    main()

