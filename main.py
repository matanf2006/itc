"""
Written bu Matan Feldman.
This module prints the preceding numbers to the one the user send as an input,
alongside their squares values.
"""
import sys
import os


def main():
    """
    Receives an integer as an input from the user as a command line argument,
    then prints all its preceding numbers alongside their square values.
    :return:
    """
    if 'param' in os.environ.keys():
        number = int(os.environ['param'])
    else:
        number = int(sys.argv[1])

    for i in range(number):
        print(i, i**2)


if __name__ == '__main__':
    main()

